﻿using System;
using System.IO;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;

namespace Framework
{
    public class Setup
    {
        public static IWebDriver GetWebDriver()
        {
            var filePath = Path.Combine(Environment.CurrentDirectory, "Drivers\\");

            if (Properties.Settings.Default["Browser"].ToString().ToLower().Equals("firefox"))
            {
                var firefoxProfile = new FirefoxOptions(); ;
                firefoxProfile.SetPreference("browser.private.browsing.autostart", true);
                return new FirefoxDriver(filePath, firefoxProfile);
            }

            if (Properties.Settings.Default["Browser"].ToString().ToLower().Equals("chrome"))
            {
                var chromeOptions = new ChromeOptions();
                chromeOptions.AddArgument("incognito");
                chromeOptions.AddArgument("--start-maximized");
                chromeOptions.AddArguments("disable-infobars");
                chromeOptions.AddArguments("--lang=" + "en-us");
                return new ChromeDriver(filePath, chromeOptions);
            }
            if (Properties.Settings.Default["Browser"].ToString().ToLower().Equals("edge"))
            {
                return new EdgeDriver(filePath);
            }

            throw new Exception("Browser is not correctly configured");
        }
    }
}
