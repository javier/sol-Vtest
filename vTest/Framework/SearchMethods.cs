﻿using OpenQA.Selenium;
using SeleniumExtras.WaitHelpers;
using System;

namespace Framework
{
    public class SearchMethods : Setup
    {
        public static IWebElement FindElement(IWebDriver driver, string id)
        {
            return new OpenQA.Selenium.Support.UI.WebDriverWait(driver, TimeSpan.FromSeconds(60)).Until(ExpectedConditions.ElementIsVisible(By.XPath(id)));
        }
        public static void WaitForPageLoad(IWebDriver driver, int timeInSeconds = 60)
        {
            var wait = new OpenQA.Selenium.Support.UI.WebDriverWait(driver, TimeSpan.FromSeconds(timeInSeconds));
            var unused = wait.Until(drv => (IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").ToString().Equals("complete");
        }
    }
}
