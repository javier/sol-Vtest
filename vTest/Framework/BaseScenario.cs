﻿using TechTalk.SpecFlow;

namespace Framework
{
    public class BaseScenario
    {
        protected BaseContext context;

        [BeforeScenario]
        public void InitializeWebDriver()
        {
            context.WebDriver = Setup.GetWebDriver();   
        }
        
        [AfterScenario]
        public void DisposeWebDriver()
        {
            context.WebDriver.Quit();
            context.WebDriver.Dispose();
        }
    }
}