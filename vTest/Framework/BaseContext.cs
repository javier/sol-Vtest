﻿using OpenQA.Selenium;

namespace Framework
{
    public class BaseContext
    {
        public IWebDriver WebDriver { get; set; }
    }
}
