﻿using Framework;
using OpenQA.Selenium;
using TechTalk.SpecFlow;
using Xunit;

namespace FunctionalTests
{
    public class DictionaryKey
    {
        public DictionaryKey(string value) => Value = value;
        public string Value { get; }
    }

    [Binding]
    public class CommonTestSteps : BaseScenario
    {
        public ScenarioContext Context { get; }
        private IWebDriver WebDriver => context.WebDriver;
        public CommonTestSteps(BaseContext baseContext, ScenarioContext scenarioContext)
        {
            this.context = baseContext;
            this.Context = scenarioContext;
        }        

        [StepArgumentTransformation]
        public DictionaryKey GetDictionaryKey(string value)
        {
            return new DictionaryKey(value.Replace(value, Mapping.Dict[value]));
        }

        [Given(@"I open ""(.*)""")]
        public void GivenIOpen(string p0)
        {
            WebDriver.Navigate().GoToUrl("https://" + p0);

        }

        [Then(@"I fill search bar with ""(.*)""")]
        public void ThenIFillSearchBarWith(string p0)
        {
            var input = SearchMethods.FindElement(context.WebDriver, ".//input[@id='lst-ib']");
            SearchMethods.FindElement(context.WebDriver, ".//input[@id='lst-ib']").SendKeys(p0);
            input.SendKeys(Keys.Escape);
        }

        [Then(@"I click on ""(.*)"" button")]
        public void ThenIClickOnButton(DictionaryKey p0)
        {
            SearchMethods.FindElement(context.WebDriver, p0.Value).Click();
        }
        [Then(@"I see page with more than ""(.*)"" results")]
        public void ThenISeePageWithMoreThanResults(int p0)
        {
            var text = SearchMethods.FindElement(context.WebDriver, ".//div[@id='resultStats']").Text.Split(' ');
            int.TryParse(text[1].Replace(".", ""), out var results);
            Assert.True(results > p0);
        }
        [Then(@"I see page with less than ""(.*)"" results")]
        public void ThenISeePageWithLessThanResults(int p0)
        {
            var text = SearchMethods.FindElement(context.WebDriver, ".//div[@id='resultStats']").Text.Split(' ');
            int.TryParse(text[1].Replace(".", ""), out var results);
            Assert.True(results < p0);
        }
        [Then(@"I check that url is ""(.*)""")]
        public void ThenICheckThatUrlIs(string p0)
        {
            SearchMethods.WaitForPageLoad(WebDriver);
            Assert.Equal(p0, WebDriver.Url);
        }
    }
}
