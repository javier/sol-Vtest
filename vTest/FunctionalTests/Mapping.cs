﻿using System.Collections.Generic;

namespace FunctionalTests
{
    public static class Mapping
    {
        public static readonly Dictionary<string, string> Dict = new Dictionary<string, string>
        {
            {"search", ".//input[@name='btnK']" },
            {"I'm feeling lucky", ".//input[@name='btnI']" },
        };
    }
}
