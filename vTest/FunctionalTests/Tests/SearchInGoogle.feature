﻿Feature: Search in google

  As an user 
  I want to be able search in google different data
  So I should be able to analyze results

  Scenario: Search "Selenium" and validate if returns more than "25000000" results
    Given I open "google.com" 
     Then I fill search bar with "Selenium"
     Then I click on "search" button
	  And I see page with more than "25000000" results

  Scenario: Search "España" and validate if returns more than "25000000" results
    Given I open "google.com" 
     Then I fill search bar with "España"
     Then I click on "search" button
	  And I see page with more than "900000000" results

  Scenario: Search "Madrid" and validate if returns less than "1000000" results
    Given I open "google.com" 
     Then I fill search bar with "Madrid"
     Then I click on "search" button
	  And I see page with less than "1000000" results

  Scenario: Search "Selenium" and validate if returns less than "10" results
    Given I open "google.es" 
     Then I fill search bar with "Selenium"
     Then I click on "search" button
	  And I see page with less than "10" results

  Scenario: Search "Selenium" using I'm feeling lucky and validate if url is https://www.seleniumhq.org/
    Given I open "google.es" 
     Then I fill search bar with "Selenium"
     Then I click on "I'm feeling lucky" button
	  And I check that url is "https://www.seleniumhq.org/"